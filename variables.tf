variable "mapping" {
  type        = map(string)
  description = "Path/worker mapping."
  default = {
    // "/-/cloudflare/hello_world" = "hello_world"
    // https://gitlab.com/gitlab-com/gl-infra/infrastructure/issues/8345
    // "/uploads/-/system/*" = "dynamic_image_resizing"
    // https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/11023
    "/" = "root_301_redirect"
  }
}

variable "cloudflare_account_id" {
  type        = string
  description = "Cloudflare account ID."
  sensitive   = true
}

variable "cloudflare_zone_name" {
  type        = string
  description = "Cloudflare zone name."
}

variable "cloudflare_zone_id" {
  type        = string
  description = "Cloudflare zone ID."
}

variable "environment" {
  type        = string
  description = "Environment name."
}
