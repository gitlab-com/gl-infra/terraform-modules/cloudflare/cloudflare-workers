addEventListener('fetch', event => {
  // if this request is coming from image resizing worker,
  // avoid causing an infinite loop by resizing it again:
  if (/image-resizing/.test(event.request.headers.get("via"))) {
    return fetch(event.request);
  }

  event.respondWith(handleRequest(event.request))
})

async function handleRequest(request) {
  let url = new URL(request.url);
  let fetchParams = {cf: {image: {fit: 'scale-down'}}};

  let width = parseInt(url.searchParams.get("width"), 10);
  if (isNaN(width) || !width) {
    return fetch(request);
  } else {
    fetchParams.cf.image.width = width;
  }

  // When we do not need the `x-with` header anymore:
  // return fetch(request, fetchParams);

  let response = await fetch(request, fetchParams);
  response = new Response(response.body, response)
  response.headers.set('x-with', 'cloudflare worker')
  return response
}
