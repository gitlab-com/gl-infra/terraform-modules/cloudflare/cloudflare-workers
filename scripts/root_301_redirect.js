// The redirect response code
const statusCode = 301
// The redirect response text
const statusText = "Moved Permanently"
// Caching Headers
const cacheControl = "no-cache, no-store, must-revalidate"
// Match request URL
const urlRegex = /^https:\/\/(staging\.)?gitlab\.com\/$/
// Worker Name
const workerName = "root_301_redirect"

async function handleRequest(request) {
  // Get originalResponse
  const originalResponse = await fetch(request)

  if (originalResponse.status == 302 && urlRegex.test(request.url)) {
    let response = new Response(originalResponse.body, {
      status: statusCode,
      statusText: statusText,
      headers: originalResponse.headers,
    })
    response.headers.set('Cache-Control', cacheControl)
    response.headers.set('gitlab-worker-response', workerName)
    return response
  }

  return originalResponse
}

addEventListener('fetch', event => {
  event.respondWith(handleRequest(event.request))
})
