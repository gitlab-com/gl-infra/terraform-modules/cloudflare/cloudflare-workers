# GitLab.com Cloudflare Workers Terraform Module

## What is this?

This module provisions Cloudflare Workers and their routes.

## What is Terraform?

[Terraform](https://www.terraform.io) is an infrastructure-as-code tool that greatly reduces the amount of time needed to implement and scale our infrastructure. It's provider agnostic so it works great for our use case. You're encouraged to read the documentation as it's very well written.

## Setup

This module is supposed to be nested in the existing gprd/gstg/etc. terraform environments. It is environment aware and avoids collisions.

The module should be included in the `main.tf` of the environment, pinned to the desired version to deploy in that environment.

An example snippet to deploy v2.0.0:

```terraform
module "cloudflare_workers" {
  source  = "ops.gitlab.net:gitlab-com/cloudflare-workers/cloudflare"
  version = "2.0.0"

  cloudflare_zone_name = var.cloudflare_zone_name
  cloudflare_zone_id   = var.cloudflare_zone_id
  environment          = var.environment
}
```

## Configuration

Configuration of the mapping should *__only__* happen within this module! This is to bind code/configuration to a version, and version to environment via pinning. This also removes drift in configuration between environments while allowing some wiggleroom during migrations.

To add/remove a worker script and/or route:

- Place the worker script in the `scripts` folder with the name `${myscript}.js`
- Edit the `variables.tf` `mapping` variable to include the route and the script (without `.js`) as follows:

  ```terraform
  variable "mapping" {
    type = map(string)
    default = {
      "/-/cloudflare/hello_world" = "hello_world"
      "/my/new/route"             = "${myscript}"
      "/my/2nd/new/route"         = "${myscript}"
    }
  }
  ```

- You can add multiple routes to the same script, as shown above.
- The route *has* to be without the FQDN and begin with a `/`. The appropriate zone will be added automatically.

## Deployment process

Opon inclusion of this module, it will deploy all scripts in the `mapping` variable to Cloudflare workers.
To do this, it will:

- Create a distinct list of all scripts in the mapping
- Create/update/destroy theses scripts in Cloudflare (on account level) and have them prefixed with the environment e.g. `gstg_example`
- Create/update/destroy all routes as portrayed in the mapping, referencing the prefixed script names.

<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.0 |
| <a name="requirement_cloudflare"></a> [cloudflare](#requirement\_cloudflare) | >= 4.39.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_cloudflare"></a> [cloudflare](#provider\_cloudflare) | >= 4.39.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [cloudflare_workers_route.route_definition](https://registry.terraform.io/providers/cloudflare/cloudflare/latest/docs/resources/workers_route) | resource |
| [cloudflare_workers_script.script_definition](https://registry.terraform.io/providers/cloudflare/cloudflare/latest/docs/resources/workers_script) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_cloudflare_account_id"></a> [cloudflare\_account\_id](#input\_cloudflare\_account\_id) | Cloudflare account ID. | `string` | n/a | yes |
| <a name="input_cloudflare_zone_id"></a> [cloudflare\_zone\_id](#input\_cloudflare\_zone\_id) | Cloudflare zone ID. | `string` | n/a | yes |
| <a name="input_cloudflare_zone_name"></a> [cloudflare\_zone\_name](#input\_cloudflare\_zone\_name) | Cloudflare zone name. | `string` | n/a | yes |
| <a name="input_environment"></a> [environment](#input\_environment) | Environment name. | `string` | n/a | yes |
| <a name="input_mapping"></a> [mapping](#input\_mapping) | Path/worker mapping. | `map(string)` | <pre>{<br>  "/": "root_301_redirect"<br>}</pre> | no |

## Outputs

No outputs.
<!-- END_TF_DOCS -->

## Contributing

Please see [CONTRIBUTING.md](./CONTRIBUTING.md).

## License

See [LICENSE](./LICENSE).
