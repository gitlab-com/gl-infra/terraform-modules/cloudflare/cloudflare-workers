locals {
  /**
   * Converts a map like:
   *
   * {
   * "/uri/path": "script_name"
   * }
   *
   * Into:
   *
   * {
   *   "gstg_script_name": "script_name"
   * }
   *
   * This makes sure, that scripts of different versions across different environments do not collide.
   * It also stabilizes to the distinct set of scripts do be deployed. This should avoid unnecessary deletions.
  */
  script_names = { for v in distinct(values(var.mapping)) : "${var.environment}_${v}" => v }
}


resource "cloudflare_workers_script" "script_definition" {
  for_each = local.script_names

  account_id = var.cloudflare_account_id
  name       = each.key
  content    = file("${path.module}/scripts/${each.value}.js")
}

resource "cloudflare_workers_route" "route_definition" {
  depends_on = [cloudflare_workers_script.script_definition]

  for_each = var.mapping

  zone_id     = var.cloudflare_zone_id
  pattern     = "${var.cloudflare_zone_name}${each.key}"
  script_name = "${var.environment}_${each.value}"
}
